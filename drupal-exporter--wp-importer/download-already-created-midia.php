<?php
if(php_sapi_name() != 'cli'){
    return;
} else {
    $_SERVER['SERVER_NAME'] = 'localhost';
}

require __DIR__ . '/../wp-load.php';
require __DIR__ . '/Encoding.php';

class SerializedDataImporter {
    static $files_url_prefix = "https://www.stopthewall.org/";

    static $data;

    static $files_url_replacement = [];
    static $files_url_attachment_ids = [];
    /**
     *
     * @var \wpdb;
     */
    static $wpdb;

    static function init() {
        wp_defer_term_counting(true);
        global $wpdb;
        self::$wpdb = $wpdb;
        self::$data = unserialize(file_get_contents(__DIR__ . '/data.serialized'));
    }

    static function log($string, $print_time = true){
        $time = $print_time ? date('Y-m-d H:i:s') . ' ' : "";
        echo  "\n{$time}{$string}\n";
    }

    static function cleanEncoding( $text, $type='standard' ){
        return ForceUTF8\Encoding::toUTF8($text);
    }

    

    

    static function sanitize_value($value) {
        return trim($value);
    }

    

    

    static function insert_attachment_from_url($url, $meta = [], $post_id = 0, $set_post_thumbnail = false) {
        $table_postmeta = self::$wpdb->postmeta;

        $attach_id = self::$wpdb->get_var("SELECT post_id FROM {$table_postmeta} WHERE meta_key = '_drupal_file_url' AND meta_value = '{$url}'");
        
        self::log('download file: ' . $url);

        $file = wp_remote_get($url, array('timeout' => 60));
        $response = wp_remote_retrieve_response_code($file);
        $body = wp_remote_retrieve_body($file);
        
        self::log("download status: $response");
        
        if ($response != 200) {
            return false;
        }
        $upload = wp_upload_bits(basename($url), null, $body);
        if (!empty($upload['error'])) {

            return false;
        }
        $file_path = $upload['file'];
        $file_name = basename($file_path);
        $file_type = wp_check_filetype($file_name, null);
        $attachment_title = sanitize_file_name(pathinfo($file_name, PATHINFO_FILENAME));
        $wp_upload_dir = wp_upload_dir();

        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . '/wp-admin/includes/media.php' );
        // Create the attachment
        self::log("File path $file_path");
        $update_status = update_attached_file($attach_id, $file_path);
        
        if($update_status == false) {
            self::log("Attachment returned false! Attachid: $attach_id Filepath: $file_path Filename: $file_name");
        }

        return $attach_id;
    }

    public static function import_files(){
        $c = 0;
        $t = count(self::$data['files_by_id']);

        // echo "rato";

        foreach(self::$data['files_by_id'] as $file){
            $c++;

            $percentage = $c/$t;
            self::log("import file $c de $t { $percentage }");

            // print_r($file);

            $url = self::$files_url_prefix . $file->filepath;
            $attach_id = self::insert_attachment_from_url($url);
            if($attach_id){
                self::$files_url_replacement["/{$file->filepath}"] = wp_get_attachment_url($attach_id);
                self::$files_url_attachment_ids["/{$file->filepath}"] = $attach_id;
            }

            // break;
        }
    }
}

ini_set('max_execution_time', -1);
ini_set('memory_limit', '2048M');

 
SerializedDataImporter::init();

SerializedDataImporter::import_files();
// SerializedDataImporter::import_users();
// SerializedDataImporter::import_posts();