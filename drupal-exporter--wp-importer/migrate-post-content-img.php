<?php
if(php_sapi_name() != 'cli'){
    return;
} else {
    $_SERVER['SERVER_NAME'] = 'localhost';
}

require __DIR__ . '/../wp-load.php';
require __DIR__ . '/Encoding.php';

class SerializedDataImporter {
    static $files_url_prefix = "https://www.stopthewall.org/";

    static $data;

    static $files_url_replacement = [];
    static $files_url_attachment_ids = [];
    /**
     *
     * @var \wpdb;
     */
    static $wpdb;

    static function init() {
        wp_defer_term_counting(true);
        global $wpdb;
        self::$wpdb = $wpdb;
        self::$data = unserialize(file_get_contents(__DIR__ . '/data.serialized'));
    }

    static function log($string, $print_time = true){
        $time = $print_time ? date('Y-m-d H:i:s') . ' ' : "";
        echo  "\n{$time}{$string}\n";
    }

    static function cleanEncoding( $text, $type='standard' ){
        return ForceUTF8\Encoding::toUTF8($text);
    }

    static function process_post($node, $valid_post_types = []) {
        // $attach_id = null;

        $dom = new DOMDocument();
        $dom->loadHTML($node->body);
        $body = $dom->getElementsByTagName('body');

        if(sizeof($dom->getElementsByTagName('img'))) {
            foreach($dom->getElementsByTagName('img') as $img) {
                $img_src = urldecode($img->getAttribute('src'));
                self::log("Prcessing image, found src: $img_src");

                if(strpos($img_src, 'sites/default') == false) {
                    self:log("Skiping: Image dosent have correct SRC pattern");
                    continue;
                }

                if(isset(self::$files_url_attachment_ids[$img_src])){
                    $new_url = self::$files_url_replacement[$img_src];
                    $img->setAttribute('src', $new_url);
                    self::log("Found attachment url for post content, new url is: $new_url");
                }
            }

            if ( $body && 0 < $body->length ) {
                $body = $body->item(0);
                $node->body = $dom->saveHTML($body);
            }
            
        };
       
        if(!in_array($node->type, $valid_post_types)){
            return [
                'skip' => true,
            ];
        }

        foreach(self::$files_url_replacement as $from => $to){
            $node->body = str_replace($from, $to, $node->body);
        }

        $props = [
            'post_content' =>  self::cleanEncoding($node->body),
        ];
        

        return array(
            'props' => $props,
            'skip' => false
        );
    }

    static function import_posts() {
        $c = 0;
        // $limited_list = array_slice(self::$data['nodes_by_id'], -100, 100, true);
        $limited_list = self::$data['nodes_by_id'];
        $t = count(self::$data['nodes_by_id']);

        $post_types = [
            'news',
            'resource',
            // 'photo',
        ];

        $should_be_skiped = [];

        $table_postmeta = self::$wpdb->postmeta;
        
        
        foreach($limited_list as $drupal_node_id => &$node){
            $c++;
            self::log("==============================================", false);
            self::log("import loop $c de $t (nid: {$node->nid} [{$node->type}]) ");
            
            if(in_array($node->nid, $should_be_skiped)) {
                continue;
            }

            $result = self::process_post($node, $post_types);
            $props = $result['props'];
            
            if($result['skip']) {
                continue;
            }

            if ($post_id = self::$wpdb->get_var("SELECT post_id FROM {$table_postmeta} WHERE meta_key = '_drupal_node_id' AND meta_value = {$node->nid}")) {
                self::log("UPDATING CONTENT post-id: {$post_id}");
                $post_id = self::updatePost($post_id, $props);
                continue;
            }

            // break;
        }
    }

    static function sanitize_value($value) {
        return trim($value);
    }

    static function updatePost($post_id, $props_values) {
        $my_post = array(
            'ID'           => $post_id,
            'post_content' => $props_values['post_content'],
        );
       
      // Update the post into the database
        wp_update_post( $my_post );

        // self::log("UPDATE: ID: $post_id - {$props_values['post_content']}");
        return $post_id;
    }

    static function insert_attachment_from_url($url, $meta = [], $post_id = 0, $set_post_thumbnail = false) {
        $table_postmeta = self::$wpdb->postmeta;

        if ($attach_id = self::$wpdb->get_var("SELECT post_id FROM {$table_postmeta} WHERE meta_key = '_drupal_file_url' AND meta_value = '{$url}'")) {
            self::log('file already downloaded: ' . $url);
            return $attach_id;
        }
        
        self::log('download file: ' . $url);

        $file = wp_remote_get($url, array('timeout' => 60));
        $response = wp_remote_retrieve_response_code($file);
        $body = wp_remote_retrieve_body($file);
        
        self::log("download status: $response");
        
        if ($response != 200) {
            return false;
        }
        $upload = wp_upload_bits(basename($url), null, $body);
        if (!empty($upload['error'])) {

            return false;
        }
        $file_path = $upload['file'];
        $file_name = basename($file_path);
        $file_type = wp_check_filetype($file_name, null);
        $attachment_title = sanitize_file_name(pathinfo($file_name, PATHINFO_FILENAME));
        $wp_upload_dir = wp_upload_dir();
        $post_info = array(
            'guid' => $wp_upload_dir['url'] . '/' . $file_name,
            'post_mime_type' => $file_type['type'],
            'post_title' => $attachment_title,
            'post_content' => '',
            'post_status' => 'inherit',
        );

        // Create the attachment
        $attach_id = wp_insert_attachment($post_info, $file_path, $post_id);
        // Include image.php
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . '/wp-admin/includes/media.php' );

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata($attach_id, $file_path);

        if($meta){
            $attach_data['image_meta']['caption'] = $meta['alt'];
            $attach_data['image_meta']['title'] = $meta['title'];
        }

        // Assign metadata to attachment
        wp_update_attachment_metadata($attach_id, $attach_data);

        if($set_post_thumbnail){
            set_post_thumbnail($post_id, $attach_id);
        }

        add_post_meta($attach_id, '_drupal_file_url', $url);

        return $attach_id;
    }

    public static function import_files(){
        $c = 0;
        $t = count(self::$data['files_by_id']);

        // echo "rato";
        $attachments_data = file_get_contents(__DIR__ . '/attachments_info.serialized');
        if($attachments_data) {
            $attachments_data = unserialize($attachments_data);
        } else {
            $attachments_data = false;
        }

        foreach(self::$data['files_by_id'] as $file){
            $c++;
            // if($c > 100) continue;

            // if(!($file->fid == 2638 || $file->fid == 2620)) {
            //     continue;
            // }

            $percentage = $c/$t;
            self::log("import file $c de $t { $percentage }");

            // print_r($file);

            $url = self::$files_url_prefix . $file->filepath;

            if(isset($attachments_data['files_url_replacement']["/{$file->filepath}"])) {
                self::log("Skiping attachment, already downloaded - using reference file for comparation");
                self::$files_url_replacement["/{$file->filepath}"] = $attachments_data['files_url_replacement']["/{$file->filepath}"];
                self::$files_url_attachment_ids["/{$file->filepath}"] = $attachments_data['files_url_attachment_ids']["/{$file->filepath}"];
                continue;
            }

            $attach_id = self::insert_attachment_from_url($url);
            if($attach_id){
                self::$files_url_replacement["/{$file->filepath}"] = wp_get_attachment_url($attach_id);
                self::$files_url_attachment_ids["/{$file->filepath}"] = $attach_id;
            }

            // break;
        }

        $attachments_data = [
            'files_url_replacement' => self::$files_url_replacement,
            'files_url_attachment_ids' => self::$files_url_attachment_ids,
        ];

        file_put_contents('attachments_info.serialized', serialize($attachments_data));
    }
}

ini_set('max_execution_time', -1);
ini_set('memory_limit', '2048M');

 
SerializedDataImporter::init();

SerializedDataImporter::import_files();
SerializedDataImporter::import_posts();