<?php
if(php_sapi_name() != 'cli'){
    return;
} else {
    $_SERVER['SERVER_NAME'] = 'localhost';
}

require __DIR__ . '/../wp-load.php';
require __DIR__ . '/Encoding.php';

class SerializedDataImporter {
    static $files_url_prefix = "https://www.stopthewall.org/";

    static $data;

    static $files_url_replacement = [];
    static $files_url_attachment_ids = [];
    /**
     *
     * @var \wpdb;
     */
    static $wpdb;

    static function init() {
        wp_defer_term_counting(true);
        global $wpdb;
        self::$wpdb = $wpdb;
        self::$data = unserialize(file_get_contents(__DIR__ . '/data.serialized'));
    }

    static function log($string, $print_time = true){
        $time = $print_time ? date('Y-m-d H:i:s') . ' ' : "";
        echo  "\n{$time}{$string}\n";
    }

    static function cleanEncoding( $text, $type='standard' ){
        return ForceUTF8\Encoding::toUTF8($text);
    }

    static function process_wpml_translation($original_post_id, $post_id, $original_language, $language, $post_type = 'post' ) {
        $wpml_element_type = apply_filters( 'wpml_element_type', $post_type );

        $get_language_args = array('element_id' => $original_post_id, 'element_type' => $wpml_element_type );
        $original_post_language_info = apply_filters( 'wpml_element_language_details', null, $get_language_args );
         
        $set_language_args = array(
            'element_id' => $post_id,
            'element_type' => $wpml_element_type,
            'language_code' => $language,
            'trid' => $original_post_language_info->trid,
            'source_language_code' => $original_language,
        );
 
        do_action( 'wpml_set_element_language_details', $set_language_args );
    }

    static function process_post($node, $valid_post_types = []) {
        $attach_id = null;

        // $dom = new DOMDocument();
        // $dom->loadHTML($node->body);

        // if(sizeof($dom->getElementsByTagName('img'))) {
        //     $featured_url = $dom->getElementsByTagName('img')->item(0)->getAttribute('src');
        //     self::log($featured_url);
        // };
        
        $featured_image_field = self::$data['featured_image_by_node_id'][$node->nid];

        if(isset($featured_image_field)) {
            self::log("Has featured image field, checking in downloaded files for attachment id (fid: $featured_image_field)");

            $file_obj = self::$data['files_by_id'][$featured_image_field];

            if(isset(self::$files_url_attachment_ids["/{$file_obj->filepath}"])){
                $attach_id = self::$files_url_attachment_ids["/{$file_obj->filepath}"];
                self::log("Attachment id found");
            } else {
                self::log("Attachment id not found");
            }
        };

            
        // if(preg_match('#<img [^>]*? =["\'](/sites[^"\']+)["\']#i',$node->body, $matches)){
        //     $url = $matches[1];
        //     self::log($url);

        //     if(isset(self::$files_url_attachment_ids[$url])){
        //         $attach_id = self::$files_url_attachment_ids[$url];
        //     }
        // } else {
        //     self::log('Regular expression didnt match');
        // }
        

        foreach(self::$files_url_replacement as $from => $to){
            $node->body = str_replace($from, $to, $node->body);
        }

        $taxonomies_terms = [];
        $metadata = [];

        if(in_array($node->type, $valid_post_types)){
            $post_type = $node->type;
            $taxonomies_terms['category'] = [ucfirst(strtolower($node->type))];
        } else {
            return [
                'skip' => true,
            ];
        }

        $user = @self::$data['users_by_id'][$node->uid];
        $post_author = @$user->WP_ID;
        
        if($node->language == 'ar') {
            $props = [
                'post_author' => $post_author,
                'post_name' => basename($node->post_name),
                'post_title' => self::cleanEncoding($node->title),
                'post_content' =>  self::cleanEncoding($node->body),
                'post_date' => date('Y-m-d h:i:s', $node->created),
                'post_modified' => date('Y-m-d h:i:s', $node->changed),
                'post_status' => $node->status == 1 ? 'publish' : 'draft'
            ];
        } else {
            $props = [
                'post_author' => $post_author,
                'post_name' => basename($node->post_name),
                'post_title' => self::cleanEncoding($node->title),
                'post_content' =>  utf8_encode($node->body),
                'post_date' => date('Y-m-d h:i:s', $node->created),
                'post_modified' => date( 'Y-m-d h:i:s', $node->changed),
                'post_status' => $node->status == 1 ? 'publish' : 'draft'
            ];
        }
        


        foreach($node->fields as $field){
            $name = $field->field_name;
            $value = null;
            if (isset($field->{"field_{$name}_value"})) {
                $value = $field->{"field_{$name}_value"};
            } elseif (isset($field->{"field_{$name}_nid"})) { 
                $value = $field->{"field_{$name}_nid"};
            } elseif (isset($field->{"field_{$name}_fid"})) { 
                $value = $field->{"field_{$name}_fid"};
            }

            $metadata[$name] = $value;
        }

        foreach($node->terms as $term){
            // TRATAR NOME DE TAXONOMIASSS!!! EXEMPLO "Subject/Topic"; -> "subject-topic"
            // Location Campaigns Events (tratar dps da migração)

            if ($term->taxonomy == 'Keywords') {
                $term->taxonomy = 'post_tag';
            }

            if(!isset($taxonomies_terms[$term->taxonomy])){
                $taxonomies_terms[strtolower($term->taxonomy)] = [];
            }

            $taxonomies_terms[strtolower($term->taxonomy)][] = $term->term;
        }

        return array(
            'props' => $props,
            'metadata' => $metadata,
            'taxonomies_terms' => $taxonomies_terms,
            'post_type' => $post_type,
            'attach_id' => $attach_id,
            'skip' => false
        );
    }

    static function import_posts() {
        $c = 0;
        // $limited_list = array_slice(self::$data['nodes_by_id'], -1300, 2, true);
        $limited_list = self::$data['nodes_by_id'];
        $t = count(self::$data['nodes_by_id']);

        $post_types = [
            'news',
            'resource',
            // 'photo',
        ];

        $should_be_skiped = [];

        $table_postmeta = self::$wpdb->postmeta;
        
        
        foreach($limited_list as $drupal_node_id => &$node){
            $c++;
            self::log("==============================================", false);
            self::log("import loop $c de $t (nid: {$node->nid} [{$node->type}]) ");
            
            if(in_array($node->nid, $should_be_skiped)) {
                continue;
            }

            // if($node->language != 'ar') {
            //     self::log("Skipping is not Arabic");
            //     continue;
            // }

            $result = self::process_post($node, $post_types);
            
            if($result['skip']) {
                continue;
            }

            $props = $result['props'];
            $metadata = $result['metadata'];
            $taxonomies_terms = $result['taxonomies_terms'];
            $post_type = $result['post_type'];
            $attach_id = $result['attach_id'];

            if ($post_id = self::$wpdb->get_var("SELECT post_id FROM {$table_postmeta} WHERE meta_key = '_drupal_node_id' AND meta_value = {$node->nid}")) {
                self::log("UPDATE SKINPING {$post_id}");
                $post_id = self::updatePost($post_id, $props, $metadata, $taxonomies_terms);
                continue;
            } else {
                self::log("INSERTING");
                $post_id = self::insert($post_type, $node->nid, $props, $metadata, $taxonomies_terms);

                if($node->tnid == $node->nid)  {
                    self::process_wpml_translation($post_id, $post_id, $node->language, $node->language, $post_type);
                } else {
                    // Pega tnid e  caça no banco o post_id do original usando mesma logica do update()
                    $original_post_id = self::$wpdb->get_var("SELECT post_id FROM {$table_postmeta} WHERE meta_key = '_drupal_node_id' AND meta_value = {$node->tnid}");
                    if(!$original_post_id) {
                        // Se não achar nada (post ainda está a frente e não foi processado (adicoinado ao wordpress)
                        $original_node = self::$data['nodes_by_id'][$node->tnid];

                        if(!isset($original_node)) {
                            self::log("Skiping: Original post missing. The translated node id is {$node->tnid}. Applying its own language relation.");
                            self::process_wpml_translation($post_id, $post_id, $node->language, $node->language, $post_type);

                            $result = self::process_post($node, $post_types);
                            $orignial_attach_id = $result['attach_id'];

                        } else {
                            $result = self::process_post($original_node, $post_types);
                            $original_props = $result['props'];
                            $original_metadata = $result['metadata'];
                            $original_taxonomies_terms = $result['taxonomies_terms'];
                            $original_post_type = $result['post_type'];
                            $orignial_attach_id = $result['attach_id'];
                            
                            $wpml_element_type = apply_filters( 'wpml_element_type', $original_post_type );

                            $original_post_id = self::insert($original_post_type, $node->nid, $original_props, $original_metadata, $original_taxonomies_terms);

                            self::process_wpml_translation($original_post_id, $original_post_id, $original_node->language, $original_node->language, $original_post_type);

                            array_push($should_be_skiped, $node->tnid);
                        }

                        if($orignial_attach_id){
                            self::log("definindo post thumbnail post_id={$original_post_id}, attach_id={$orignial_attach_id}");
                            set_post_thumbnail($original_post_id, $orignial_attach_id);
                        }

                    }

                    $wpml_element_type = apply_filters( 'wpml_element_type', $post_type);
                    $get_language_args = array('element_id' => $original_post_id, 'element_type' => $wpml_element_type );
                    $original_post_language_info = apply_filters( 'wpml_element_language_details', null, $get_language_args );

                    self::process_wpml_translation($original_post_id, $post_id, $original_post_language_info->language_code, $node->language, $post_type);                        
                }
            }

            if($attach_id){
                self::log("definindo post thumbnail post_id={$post_id}, attach_id={$attach_id}");
                set_post_thumbnail($post_id, $attach_id);
            }

            // break;
        }
    }

    static function sanitize_value($value) {
        return trim($value);
    }

    static function insert_postmeta($post_id, $metadata) {

        foreach ($metadata as $key => $val) {
            if (!is_array($val)) {
                $val = [$val];
            }

            foreach ($val as $value) {
                $value = self::sanitize_value($value);
                self::$wpdb->insert(self::$wpdb->postmeta, [
                    'post_id' => $post_id,
                    'meta_key' => $key,
                    'meta_value' => $value,
                ]);
            }
        }
    }

    /**
     * Insere um post na tabela wp_posts
     *
     * @param string $post_type Post Type destino
     * @param int $drupal_node_id id na tabela de origem do banco de dados original
     * @param array $props_values valores da tabela wp_posts
     * @param array $metadata valores para serem inseridos na tabela wp_postmeta
     * @param array $terms termos de taxonomias
     *
     * @return int id do post inserido
     */
    static function insert($post_type, $drupal_node_id, $props_values, $metadata = [], $taxonomies_terms = []) {
        self::log(__METHOD__);

        $metadata['_drupal_node_id'] = $drupal_node_id;


        $args = $props_values + [
            'post_type' => $post_type,
            'post_status' => 'publish',
        ];


        $post_id = wp_insert_post($args, true);
        
        if(is_object($post_id)){
            var_dump($post_id);
        }

        $termos = '';

        if (is_numeric($post_id)) {
            self::insert_postmeta($post_id, $metadata);

            foreach ($taxonomies_terms as $taxonomy => $_terms) {
                if(!taxonomy_exists($taxonomy)){
                    // self::log('Taxonomy ' . $taxonomy . " dosent exist");
                    register_taxonomy($taxonomy, $post_type);
                }

                // self::log('Taxonomy ' . $taxonomy . " exist");

                $terms = [];
                foreach ($_terms as $i => $term) {
                    $term = trim($term);
                    if (!$term) {
                        continue;
                    }
                    $terms[$i] = $term;
                }

                if ($terms) {
                    $termos .= $taxonomy . ' (' . implode(';', $terms) . ') ';
                    $result = wp_set_object_terms($post_id, $terms, $taxonomy);
                }

                print_r($terms);
            }
        }
        self::log("INSERT: ID: $post_id (nid: $drupal_node_id) - {$args['post_title']} - $termos");

        return $post_id;
    }

    static function updatePost($post_id, $props_values, $metadata = [], $terms = []) {
        /*
        foreach ($metadata as $key => $val) {
            self::$wpdb->query("DELETE FROM wp_postmeta WHERE meta_key = '$key' AND post_id = $post_id AND meta_key <> 'panels_data'");
        }

        self::insert_postmeta($post_id, $metadata);

        $termos = '';
        foreach ($terms as $taxonomy => $terms) {
            $termos .= $taxonomy . ' (' . implode(';', $terms) . ') ';
            wp_set_object_terms($post_id, $terms, $taxonomy);
        }

        self::$wpdb->update(self::$wpdb->posts, $props_values, 'ID = ' . $post_id);
        */
        self::$wpdb->query("UPDATE " . self::$wpdb->posts . " SET post_name = '{$props_values['post_name']}' WHERE ID = $post_id");

        self::log("UPDATE: ID: $post_id - {$props_values['post_title']}");
        return $post_id;
    }

    static function insert_attachment_from_url($url, $meta = [], $post_id = 0, $set_post_thumbnail = false) {
        $table_postmeta = self::$wpdb->postmeta;

        if ($attach_id = self::$wpdb->get_var("SELECT post_id FROM {$table_postmeta} WHERE meta_key = '_drupal_file_url' AND meta_value = '{$url}'")) {
            self::log('file already downloaded: ' . $url);
            return $attach_id;
        }
        
        self::log('download file: ' . $url);

        $file = wp_remote_get($url, array('timeout' => 60));
        $response = wp_remote_retrieve_response_code($file);
        $body = wp_remote_retrieve_body($file);
        
        self::log("download status: $response");
        
        if ($response != 200) {
            return false;
        }
        $upload = wp_upload_bits(basename($url), null, $body);
        if (!empty($upload['error'])) {

            return false;
        }
        $file_path = $upload['file'];
        $file_name = basename($file_path);
        $file_type = wp_check_filetype($file_name, null);
        $attachment_title = sanitize_file_name(pathinfo($file_name, PATHINFO_FILENAME));
        $wp_upload_dir = wp_upload_dir();
        $post_info = array(
            'guid' => $wp_upload_dir['url'] . '/' . $file_name,
            'post_mime_type' => $file_type['type'],
            'post_title' => $attachment_title,
            'post_content' => '',
            'post_status' => 'inherit',
        );

        // Create the attachment
        $attach_id = wp_insert_attachment($post_info, $file_path, $post_id);
        // Include image.php
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . '/wp-admin/includes/media.php' );

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata($attach_id, $file_path);

        if($meta){
            $attach_data['image_meta']['caption'] = $meta['alt'];
            $attach_data['image_meta']['title'] = $meta['title'];
        }

        // Assign metadata to attachment
        wp_update_attachment_metadata($attach_id, $attach_data);

        if($set_post_thumbnail){
            set_post_thumbnail($post_id, $attach_id);
        }

        add_post_meta($attach_id, '_drupal_file_url', $url);

        return $attach_id;
    }

    public static function import_files(){
        $c = 0;
        $t = count(self::$data['files_by_id']);

        // echo "rato";

        foreach(self::$data['files_by_id'] as $file){
            $c++;
            // if($c > 100) continue;

            // if(!($file->fid == 2638 || $file->fid == 2620)) {
            //     continue;
            // }

            $percentage = $c/$t;
            self::log("import file $c de $t { $percentage }");

            // print_r($file);

            $url = self::$files_url_prefix . $file->filepath;
            $attach_id = self::insert_attachment_from_url($url);
            if($attach_id){
                self::$files_url_replacement["/{$file->filepath}"] = wp_get_attachment_url($attach_id);
                self::$files_url_attachment_ids["/{$file->filepath}"] = $attach_id;
            }

            // break;
        }
    }

    public static function import_users(){
        $wpdb = self::$wpdb;

        foreach(self::$data['users_by_id'] as $uid => &$user){
            $user->email = trim($user->email);

            if(!$user->mail){
                continue;
            }

            if ($user_id = $wpdb->get_var("SELECT ID FROM $wpdb->users WHERE user_email = '$user->mail'")) {
                self::log("usuário {$user->name} <{$user->mail}> já importado");
                $user->WP_ID = $user_id;

            } else {
                self::log("importando usuário {$user->name} <{$user->mail}>");

                $user_id = wp_insert_user([
                    'user_email' => $user->email,
                    'user_login' => $user->email,
                    'user_pass' => null,
                    'display_name' => $user->name
                ]);

                $wpdb->query("UPDATE $wpdb->users SET user_pass = '$user->pass' WHERE user_login = '$user->mail'");

                $wp_user = new \WP_User( $user_id );
                $wp_user->set_role( 'administrator' );  

                add_user_meta($user_id, 'LEGACY:uid', $user->uid);

                $user->WP_ID = $user_id;
            }
        }
    }

}

ini_set('max_execution_time', -1);
ini_set('memory_limit', '2048M');

 
SerializedDataImporter::init();

SerializedDataImporter::import_files();
// SerializedDataImporter::import_users();
SerializedDataImporter::import_posts();