<?php
//require __DIR__ . '/vendor/autoload.php';
class Drupal2JSON {
    protected $dbhost = 'mariadbvelho';
    protected $dbname = 'stw_drup';
    protected $dbuser = 'root';
    protected $dbpass = 'therootpassword';
    
    protected $dbh = null;

    protected $schema = [];

    protected $data = [];
    
    function __construct($dbhost = 'mariadbvelho', $dbname = 'stw_drupal', $dbuser = 'root', $dbpass = 'therootpassword', $dbport = 3306) {
        $this->dbhost = $dbhost;
        $this->dbname = $dbname;
        $this->dbuser = $dbuser;
        $this->dbpass = $dbpass;
        
        try{

            $this->dbh = new PDO("mysql:dbname=$dbname;host=$dbhost", $dbuser, $dbpass);
        } catch (Exception $e){
            var_dump($e);
        }
    }

    function fetch($sql, $fetch_type = null, $key_prop = null){
        echo "\n$sql\n";
        $query = $this->dbh->query($sql);
        if($query){
            $result = $query->fetchAll($fetch_type ? : PDO::FETCH_OBJ);
        } else {
            $result = [];
        }
        
        if($key_prop){
            $rs = [];
            foreach($result as $obj){
                $rs[$obj->$key_prop] = $obj;
            }

            return $rs;
            
        }

        return $result;
    }

    function fetchSchema(){
        $types = $this->fetch("SELECT * FROM node_type WHERE module = 'node'");
        
        foreach($types as $type){
            $type->fields = [];
            $type->groups = [];

            $fields = $this->fetch("SELECT field_name AS name FROM content_node_field_instance WHERE type_name = '{$type->type}'");

            $groups = [];
            $fields_groups = [];

            foreach($fields as $field){
                $type->fields[$field->name] = $field;
            }
            
            $this->schema[$type->type] = $type;
        }
    }

    function fetchData(){
        $fields = [];


        $users = $this->fetch("SELECT * FROM users", null, 'uid');

        $field_types = $this->fetch("SELECT DISTINCT(field_name) as name FROM content_node_field_instance");

        foreach($field_types as $field){
            $field_name = $field->name;
            $table = "content_{$field_name}";
            $values = $this->fetch("SELECT * FROM {$table}");

            foreach($values as $val){
                $val->field_name = substr($field_name, 6);
                $v = clone $val;
                unset($v->nid, $v->vid);
                if(@isset($fields[$val->nid][$val->vid][$field_name])){
                    if(is_array($fields[$val->nid][$val->vid][$field_name])){
                        $fields[$val->nid][$val->vid][$field_name][] = $v;
                    } else {
                        $fields[$val->nid][$val->vid][$field_name] = [
                            $fields[$val->nid][$val->vid][$field_name],
                            $v
                        ];
                    }
                } else {
                    $fields[$val->nid][$val->vid][$field_name][] = $v;
                }
                @$fields[$val->nid][$val->vid][$field_name] = $v;
            }
        }

        $locations = $this->fetch("SELECT * FROM location", null, 'lid');
        $files = $this->fetch("SELECT * FROM files", null, 'fid');

        $nodes = $this->fetch("
            SELECT 
                n.nid, n.vid, n.uid, n.type, n.language, n.tnid, nr.title, nr.body, n.created, n.changed, n.status
            FROM
                 node n 
            LEFT JOIN 
                node_revisions nr 
            ON 
                nr.nid = n.nid AND nr.vid = n.vid 
            ORDER BY n.nid ASC,n.vid ASC");

        $urls = [];
        $redirects = [];
        foreach($this->fetch("SELECT src as source, dst as alias FROM url_alias WHERE src LIKE 'node/%' ORDER BY pid") as $alias){
            if(isset($urls[$alias->source])){
                if(!isset($redirects[$alias->source])){
                    $redirects[$alias->source] = [];
                }
                $redirects[$alias->source][] = $urls[$alias->source];
            }
            $urls[$alias->source] = $alias->alias;
        }

        foreach($nodes as &$node){
            $node->terms = $this->fetch("
                SELECT 
                    v.name as taxonomy, 
                    t.name as term 
                FROM vocabulary v 
                    JOIN term_data t 
                        ON t.vid = v.vid 
                    JOIN term_node tn 
                        ON t.tid = tn.tid 
                WHERE nid = {$node->nid}");

            $node->user = @$users[$node->uid];

            $node->post_name = @$urls["node/{$node->nid}"];
            
            $node->redirects = @$urls["node/{$node->nid}"];

            if(!isset($this->data['nodes_by_type'][$node->type])){
                $this->data['nodes_by_type'][$node->type] = [];
            }

            $this->data['nodes_by_type'][$node->type][$node->nid] = $node;
            $this->data['nodes_by_id'][$node->nid] = $node;
            $node->fields = (object)[];
            if(isset($fields[$node->nid][$node->vid])){
                foreach($fields[$node->nid][$node->vid] as $field_name => $val){
                    $def = @$this->schema[$node->type]->fields[$field_name];

                    if(!$def){
                        continue;
                        die(var_dump($this->schema[$node->type]->fields,$field_name));
                    }
                    
                    $node->fields->$field_name = $val;
                }
            }

            // $featured_image_id = 

            // if($featured_image_id) {
            //     $this->data['featured_image_by_node_id'][$node->nid] = $featured_image_id;
            // }
            if(isset($node->fields->field_photo->field_photo_nid)) {
                $node_id = $node->fields->field_photo->field_photo_nid;
                foreach($this->fetch("SELECT field_image_fid FROM content_type_photo WHERE nid = $node_id") as &$item) {
                    $this->data['featured_image_by_node_id'][$node->nid] = $item->field_image_fid;
                }
            }

            $node->images = [];
            foreach($this->fetch("SELECT fid, image_size FROM image WHERE nid = $node->nid") as &$img){
                $img->file = @$files[$img->fid];
                $node->images[] = $img;
            }

        }

        $this->data['files_by_id'] = $files;
        $this->data['locations_by_id'] = $locations;
        $this->data['users_by_id'] = $users;

    }

    public static function convert_from_latin1_to_utf8_recursively($dat) {
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[ $i ] = self::convert_from_latin1_to_utf8_recursively($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

            return $dat;
        } else {
            return $dat;
        }
    }

    function saveExpotedData(){

                
        // Sample use
        // Just pass your array or string and the UTF8 encode will be fixed
        // $data = self::convert_from_latin1_to_utf8_recursively($this->data);

        
        // $json_data = json_encode($data, JSON_UNESCAPED_UNICODE);
        file_put_contents('data.serialized', serialize($this->data));

        echo "\ndone!\n";
    }
}

$db = new Drupal2JSON('mariadbvelho', 'stw_drupal', 'root', 'therootpassword', 3306);

$db->fetchSchema();
$db->fetchData();
$db->saveExpotedData();