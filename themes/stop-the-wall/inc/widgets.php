<?php

/**
 * Register our sidebars, widgetized areas and widgets.
 *
 */
function widgets_areas() {
	
	register_sidebar(array(
		'name'          => 'Footer column 1',
		'id'            => 'footer_column1_sidebar',
		'before_widget' => '<div class="footer-column1-sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

	register_sidebar(array(
		'name'          => 'Footer column 2',
		'id'            => 'footer_column2_sidebar',
		'before_widget' => '<div class="footer-column2-sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

	register_sidebar(array(
		'name'          => 'Footer column 3',
		'id'            => 'footer_column3_sidebar',
		'before_widget' => '<div class="footer-column3-sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

	register_sidebar(array(
		'name'          => 'Footer column 4',
		'id'            => 'footer_column4_sidebar',
		'before_widget' => '<div class="footer-column4-sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));
	
	register_sidebar(array(
		'name'          => 'Article below author info',
		'id'            => 'after_post_widget_area',
		'before_widget' => '<div class="widget-area-after-post">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar(array(
		'name'          => 'Category page sidebar',
		'id'            => 'category_page_sidebar',
		'before_widget' => '<div class="widget-category_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));



	register_sidebar(array(
		'name'          => 'Search page sidebar',
		'id'            => 'search_page_sidebar',
		'before_widget' => '<div class="widget-search_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));


	register_sidebar(array(
		'name'          => 'Resources page sidebar',
		'id'            => 'resources_page_sidebar',
		'before_widget' => '<div class="widget-resources_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title' => '<!--',
		'after_title' => '-->',
	));

	register_sidebar(array(
		'name'          => 'Author page sidebar',
		'id'            => 'author_page_sidebar',
		'before_widget' => '<div class="widget-author_page_sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="author_page_sidebar">',
		'after_title'   => '</h2>',
	));

	register_sidebar(array(
		'name'          => 'Republish modal bullets',
		'id'            => 'republish_modal_bullets',
		'before_widget' => '<div class="widget-area-after-post">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action('widgets_init', 'widgets_areas');


// Creates  widgets
class newsletter_widget extends WP_Widget {

	// The construct part  
	function __construct() {
		parent::__construct(
			'newsletter_widget',
			__('Newsletter', 'jeo'),
			array('description' => __('Newsletter widget', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
?>
	<?php if ($instance) : ?>
		<div class="category-page-sidebar">
			<div class="newsletter <?= _e($instance['model_type'], 'jeo') ?> <?= $instance['custom_style'] ?>">
				<div class="newsletter-section-header">
					<div>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" data-inline="false" data-width="1em" data-height="1em" data-icon="simple-line-icons:envelope-open" style="transform: rotate(360deg);"><path d="M1023 473q0-4-1-8t-2.5-8t-3.5-7.5t-5-6.5l-1-1q-2-3-5-6l-6-6L582 27Q555 0 514 0t-68 27L29 427q-6 6-12 13T5.5 458.5T0 481v479q0 27 19 45.5t45 18.5h896q17 0 32-8.5t23.5-23.5t8.5-32V478l-1-5zM270 726L64 916V545zm66 27l6-6l1-1l141-130q11-9 25-9t24 8l384 345H110zm421-22l203-180v362zM104 443L491 72q10-9 23-9t23 9l361 345h-1l55 55l-242 215l-136-121q-18-15-41-20t-47 0t-43 21L317 683L75 469l27-26h2z" fill="currentColor"></path></svg>
					</div>
					<div class="left">
						<div class="newsletter-header">
							<p><?= _e($instance['title'], 'jeo') ?></p>
						</div>
						<p class="anchor-text">
							<?= _e($instance['subtitle'], 'jeo') ?>
						</p>
					</div>
				</div>
				<?= ($instance['model_type'] == 'horizontal') ? '<div class="input-div">' : '' ?>
				<?php if (!empty($instance['newsletter_shortcode'])) : ?>
					<?= do_shortcode($instance['newsletter_shortcode']) ?>
				<?php endif; ?>
				<?php if (!empty($instance['adicional_content'])) : ?>
					<p class="link-add"><?= _e($instance['adicional_content'], 'jeo') ?></p>
				<?php endif; ?>
				<?= ($instance['model_type'] == 'horizontal') ? '</div>' : '' ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$subtitle = !empty($instance['subtitle']) ? $instance['subtitle'] : esc_html__('', 'jeo');
		$newsletter_shortcode = !empty($instance['newsletter_shortcode']) ? $instance['newsletter_shortcode'] : esc_html__('', 'jeo');
		$adicional_content = !empty($instance['adicional_content']) ? $instance['adicional_content'] : esc_html__('', 'jeo');
		$model_type = !empty($instance['model_type']) ? $instance['model_type'] : esc_html__('vertical', 'jeo');
		$custom_style = !empty($instance['custom_style']) ? $instance['custom_style'] : esc_html__('', 'jeo');
	?>
		<p>
			<?= _e('You are not allowed to add HTML in any of those fields', 'jeo') ?>
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('model_type')); ?>"><?php esc_attr_e('Model type:', 'jeo'); ?></label>
			<select class="widefat" id="<?php echo esc_attr($this->get_field_id('model_type')); ?>" name="<?php echo esc_attr($this->get_field_name('model_type')); ?>">
				<option value="horizontal" <?= $model_type == 'horizontal' ? 'selected' : '' ?>><?php _e('Horizontal', 'jeo') ?></option>
				<option value="vertical" <?= $model_type == 'vertical' ? 'selected' : '' ?>><?php _e('Vertical', 'jeo') ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"><?php esc_attr_e('Subtitle:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>" type="text" value="<?php echo esc_attr($subtitle); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('newsletter_shortcode')); ?>"><?php esc_attr_e('Newsletter form shortcode:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('newsletter_shortcode')); ?>" name="<?php echo esc_attr($this->get_field_name('newsletter_shortcode')); ?>" type="text" value="<?php echo esc_attr($newsletter_shortcode); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('adicional_content')); ?>"><?php esc_attr_e('Additional Content:', 'jeo'); ?></label>
			<textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('adicional_content')); ?>" name="<?php echo esc_attr($this->get_field_name('adicional_content')); ?>"><?php echo $adicional_content; ?></textarea>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('custom_style')); ?>"><?php esc_attr_e('Container style:', 'jeo'); ?></label>
			<textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('custom_style')); ?>" name="<?php echo esc_attr($this->get_field_name('custom_style')); ?>"><?php echo $custom_style; ?></textarea>
		</p>


	<?php
	}
}

class social_networks extends WP_Widget {

	// The construct part  
	function __construct() {
		parent::__construct(
			'social_networks',
			__('Social networks', 'jeo'),
			array('description' => __('Social networks', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
	if ($instance) : ?>
		<div class="social-networks-widget">
			<div class="social-networks-widget-header">
				<h3>  <?= $instance['title'] ?> </h3>
				<span>  <?= $instance['description'] ?> </span>
			</div>

			<div class="social-networks-widget-content">
				<?php /*get_template_part( 'template-parts/content/content', 'social-networks' ); */ ?>
				<div class="page--share">
					<?php newspack_social_menu_header() ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$description = !empty($instance['description']) ? $instance['description'] : esc_html__('', 'jeo');
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('description')); ?>"><?php esc_attr_e('Description:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('description')); ?>" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="text" value="<?php echo esc_attr($description); ?>">
		</p>

	<?php
	}
}

class bullet_widget extends WP_Widget {

	// The construct part  
	function __construct() {
		parent::__construct(
			'bullet_widget',
			__('Bullet', 'jeo'),
			array('description' => __('Bullet widget', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
?>
	<?php if ($instance) : ?>
	<?php endif; ?>
	<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$description = !empty($instance['description']) ? $instance['description'] : esc_html__('', 'jeo');
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('description')); ?>"><?php esc_attr_e('Description:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('description')); ?>" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="textarea" value="<?php echo esc_attr($description); ?>">
		</p>
	<?php
	}
}

class most_read_widget extends WP_Widget {

	// The construct part  
	function __construct() {
		parent::__construct(
			'most_read_widget',
			__('Most Read', 'jeo'),
			array('description' => __('Most Read Widget', 'jeo'),)
		);
	}

	public function widget($args, $instance) {
		$category = '';
		$tag = '';
		$author = '';
		$most_read = [];
		$ids = [];
		$posts_ids = [];
		$posts_query_args = [];

		if(is_category()) {
			global $wp_query;

			$category = $wp_query->get_queried_object();
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
			$posts_query_args['category__in'] = [$category->term_id];
		} else if(is_tag()) {
			$tag = get_queried_object();
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
			$posts_query_args['tag__in'] = [$tag->term_id];
		} else if(is_author()) {
			$author = get_the_author_meta('ID');
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
			$posts_query_args['author__in'] = [$author];
		} else {
			$most_read = \PageViews::get_top_viewed(-1, ['post_type' => 'post', 'from' => '01-01-2001']);
		}

		$ids = array();
		foreach ($most_read as $post => $value) {
			array_push($ids, $value->post_id);
		}

		$posts_query_args['post__in'] = $ids;
		$posts_query_args['orderby'] = 'post__in';
		$most_read_query = new \WP_Query($posts_query_args); 

		foreach ($most_read_query->posts as $post => $value) {
			array_push($posts_ids, $value->ID);
		}

		?>
			<?php if($instance): ?>
				<?php if(sizeof($posts_ids) >= $instance['min_posts']): ?>
					<div class="category-most-read">
						<div class="most-read-header">
							<p><?= $instance['title'] ?> </p>
						</div>
						<div class="posts">
							<?php foreach(array_slice($posts_ids, 0, $instance['max_posts']) as $key=>$value){ 
								$title = get_the_title($value);
								$url = get_permalink($value);
							?>
								<div class="post">
										<?php if($instance['featured_image'] == 'show' && has_post_thumbnail($value)): ?>
											<a href="<?php echo $url; ?>" style="width: 46%;">
												<div class="post-thumbnail"><?php echo get_the_post_thumbnail($value); ?></div>
											</a>
											<div class="entry-container" style="width: 53%;">
												<?php newspack_categories_byId($value); ?>
												<a href="<?php echo $url; ?>">
													<p class="post-title"><?php echo $title; ?></p>
												</a>
											</div>
										<?php else: ?>
											<div class="entry-container">
												<?php newspack_categories_byId($value); ?>
												<a href="<?php echo $url; ?>">
													<p class="post-title"><?php echo $title; ?></p>
												</a>
											</div>
										<?php endif; ?>
								</div>
							<?php } ?>
						</div>
					<?php endif ?>

				</div>
			<?php endif ?>
		<?php
	}

	public function form($instance) {
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('', 'jeo');
		$min_posts = !empty($instance['min_posts']) ? $instance['min_posts'] : 1;
		$max_posts = !empty($instance['max_posts']) ? $instance['max_posts'] : 3;
		$featured_image = !empty($instance['featured_image']) ? $instance['featured_image'] : esc_html__('show', 'jeo');

	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('min_posts')); ?>"><?php esc_attr_e('Mininum quantity of posts needed to show the widget:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('min_posts')); ?>" name="<?php echo esc_attr($this->get_field_name('min_posts')); ?>" type="number" value="<?php echo esc_attr($min_posts); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('max_posts')); ?>"><?php esc_attr_e('Maximum quantity of posts shown in the widget:', 'jeo'); ?></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id('max_posts')); ?>" name="<?php echo esc_attr($this->get_field_name('max_posts')); ?>" type="number" value="<?php echo esc_attr($max_posts); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('featured_image')); ?>"><?php esc_attr_e('Featured image setting:', 'jeo'); ?></label>
			<select class="widefat" id="<?php echo esc_attr($this->get_field_id('featured_image')); ?>" name="<?php echo esc_attr($this->get_field_name('featured_image')); ?>">
				<option value="show" <?= $featured_image == 'show' ? 'selected' : '' ?>>Show featured image</option>
				<option value="hide" <?= $featured_image == 'hide' ? 'selected' : '' ?>>Hide featured image</option>
			</select>
		</p>
	<?php
	}
}

function my_post_gallery_widget($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

	$output .= "<div class=\"image-gallery\">";
	$output .= "<div class=\"image-gallery-header\"><p>";
	$output .= $attr['title'];
	$output .= "</p></div>";
    $output .= "<div class=\"image-gallery-content-block wp-block-gallery columns-3 is-cropped\"><div class=\"blocks-gallery-grid\">";

    foreach ($attachments as $id => $attachment) {
        $img = wp_get_attachment_image_src($id, 'full');

        $output .= "<div class=\"blocks-gallery-item\">\n";
        $output .= "<img src=\"{$img[0]}\"/>\n";
        $output .= "</div>\n";
    }

	$output .= "</div></div>\n";
	$output .= "<button><a target=\"blank\" href=\"";
	$output .= $attr['see_more_url'];
	$output .= "\">SEE MORE</a></button>\n";

	$output .= "</div>\n";


    return $output;
}

function newsletter_load_widget() {
	register_widget('newsletter_widget');
}

function social_networks_load_widget() {
	register_widget('social_networks');
}

function most_read_load_widget() {
	register_widget('most_read_widget');
}

function bullet_load_widget() {
	register_widget('bullet_widget');
}


add_action( 'widgets_init', 'social_networks_load_widget' );
add_action( 'widgets_init', 'newsletter_load_widget' );
add_action( 'widgets_init', 'most_read_load_widget' );
add_action( 'widgets_init', 'bullet_load_widget' );
add_filter('post_gallery', 'my_post_gallery_widget', 10, 2);


// IMAGE GALLERY FORM
function image_gallery_form( $widget, $return, $instance ) {
 
    if ( 'media_gallery' == $widget->id_base ) {
 
        $see_more_url = isset( $instance['see_more_url'] ) ? $instance['see_more_url'] : '';
        ?>
            <p>
                <label for="<?php echo $widget->get_field_id('see_more_url'); ?>">
                    <?php _e( 'See more URL (requires https://)', 'image_gallery' ); ?>
                </label>
                <input class="text" value="<?php echo $see_more_url ?>" type="text" id="<?php echo $widget->get_field_id('see_more_url'); ?>" name="<?php echo $widget->get_field_name('see_more_url'); ?>" />
            </p>
        <?php
    }
}

function widget_save_form( $instance, $new_instance ) {
	
	$instance['see_more_url'] = $new_instance['see_more_url'];
	return $instance;
}

add_filter('in_widget_form', 'image_gallery_form', 10, 3 );
add_filter( 'widget_update_callback', 'widget_save_form', 10, 2 );
add_action('widgets_init', 'newsletter_load_widget');
add_action('widgets_init', 'most_read_load_widget');
?>