<?php
$taxonomy = 'resource_type';
?>

<div class="filtro-posts">
    <div class="categories">
        <a href="#" class="active" data-id="-1" data-taxonomy="<?= $taxonomy ?>"><?= __('All', 'jeo') ?></a>
        <?php
        $terms = get_terms($taxonomy);
        foreach ($terms as $term) :
            $count = get_count_posts(get_post_type(), $taxonomy, $term->term_id);
            if($count >= 1) : ?>
            <a data-id="<?= $term->term_id ?>" data-count="<?= $count ?>" data-taxonomy="<?= $taxonomy ?>">
                <?= $term->name ?>
            </a>
        <?php endif;
        endforeach;
        ?>
    </div>
</div>

<div class="content-options">
    <div class="posts-quantity">
        <?= __('Showing','jeo') ?>
        <span class="actual-number"> <strong> 0 </strong>
        <?= __('of','jeo') ?>
        <span class="total">
            <strong> 
            <?php 
                global $wp_query;
                echo $wp_query->found_posts;
            ?>
            </strong>
        </span>
        <?= __('posts','jeo') ?>
    </div>

    <label for="sort-options" class="filter-options">
        <?= __('Sort by:') ?>
        <select name="sort" id="sort-options">
            <option value="date-newest">
                <?= __('Newest','jeo') ?>
            </option>
            <option value="date-oldest">
                <?= __('Oldest', 'jeo') ?>
            </option>
        </select>
    </label>
</div>

<div class="posts-list horizontal">
    <div class="posts-list--content search-results">

    </div>
    <div class="loading-posts">
        <img src=<?= get_stylesheet_directory_uri() . '/assets/gifs/loading.gif' ?> />

        <span><?= __('Loading more posts...', 'jeo') ?></span>
    </div>
</div>
