<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newspack
 */

global $post;
$terms = wp_get_post_terms( $post->ID, 'resource_type', array( 'fields' => 'all' ) );
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php newspack_post_thumbnail(); ?>

	<div class="entry-container">
		<?php if ( 'page' !== get_post_type() ) : ?>
			<span class="cat-links">
				<?php foreach($terms as $key=>$value): ?>
					<span class="screen-reader-text"><?= __('Posted in', 'jeo') ?></span>
					<a href="<?php echo get_term_link($value->slug, 'resource_type'); ?>"><?php echo $value->name; ?></a>

					<?php if($key < count($terms) - 1): ?>
						<span class="separator">, </span>
					<?php endif ?>
				<?php endforeach; ?>
			</span>
		<?php endif; ?>
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		</header><!-- .entry-header -->

		<?php if ( 'page' !== get_post_type() ) : ?>
			<div class="entry-meta">
				<?php newspack_posted_on(); ?>
			</div><!-- .meta-info -->
		<?php endif; ?>

		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div><!-- .entry-content -->
	</div><!-- .entry-container -->
</article><!-- #post-${ID} -->

