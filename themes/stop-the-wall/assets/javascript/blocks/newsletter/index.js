import {
  RichText,
  InnerBlocks,
} from "@wordpress/block-editor";

import { __ } from "@wordpress/i18n";
import { Button, SelectControl, TextControl } from "@wordpress/components";
//const {  } = wp.editor;

wp.blocks.registerBlockType("jeo-theme/custom-newsletter-block", {
  title: "Newsletter",
  icon: "email",
  category: "common",
  supports: {
    align: true,
  },
  attributes: {
    title: {
      type: "string",
    },

    typeNews: {
      type: "string",
    },
    subtitle: {
      type: "string",
    },

    newsletterShortcode: {
      type: "string",
    },

    lastEditionLink: {
      type: "string",
    },

    adicionalContent: {
      type: "string",
    },

    customStyle: {
      type: "string",
    },
  },

  edit: (props) => {
    const {
      className,
      isSelected,
      attributes: {
        title,
        subtitle,
        newsletterShortcode,
        adicionalContent,
        customStyle,
        typeNews,
      },
      setAttributes,
    } = props;

    return (
      <>
        <div className="newsletter-wrapper" key="container">
          <div class="category-page-sidebar">
          <SelectControl
              label={ __( 'Select newsletter type:', 'jeo' ) }
              value={ typeNews }
              onChange={ (value) => {setAttributes( { typeNews: value } ) } }
              options={ [
                  { value: null, label: 'Select a type', disabled: true },
                  { value: 'horizontal', label: 'Horizontal' },
                  { value: 'vertical', label: 'Vertical' },
              ] }
          />
          <TextControl
            label={ __( 'Add custom css:', 'jeo' ) }
            value={ customStyle }
            onChange={ ( value ) => {setAttributes( { customStyle: value } ) } }
          />
            <div class="newsletter">
              <div class="newsletter-section-header">
              <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" data-inline="false" data-width="1em" data-height="1em" data-icon="simple-line-icons:envelope-open" style={ { transform: 'rotate(360deg)' } }><path d="M1023 473q0-4-1-8t-2.5-8t-3.5-7.5t-5-6.5l-1-1q-2-3-5-6l-6-6L582 27Q555 0 514 0t-68 27L29 427q-6 6-12 13T5.5 458.5T0 481v479q0 27 19 45.5t45 18.5h896q17 0 32-8.5t23.5-23.5t8.5-32V478l-1-5zM270 726L64 916V545zm66 27l6-6l1-1l141-130q11-9 25-9t24 8l384 345H110zm421-22l203-180v362zM104 443L491 72q10-9 23-9t23 9l361 345h-1l55 55l-242 215l-136-121q-18-15-41-20t-47 0t-43 21L317 683L75 469l27-26h2z" fill="currentColor"></path></svg>
                <div class="newsletter-header">
                  
                    <RichText
                      tagName="p"
                      placeholder={__("Title", 'jeo')}
                      value={title}
                      onChange={(value) => setAttributes({ title: value })}
                    />
                  
                </div>

                <div class="customized-content">
                    <RichText
                        tagName="p"
                        className="anchor-text"
                        placeholder={__("Subtitle", 'jeo')}
                        value={subtitle}
                        onChange={(value) => setAttributes({ subtitle: value })}
                    />
                </div>

              </div>

              <div>
                <InnerBlocks
                    allowedBlocks={['core/shortcode']}
                    template={[['core/shortcode', {placeholder: 'Newsletter shortcode'}]]}
			    />
                <RichText
                        tagName="p"
                        className="link-add"
                        placeholder={__("Additional Information", 'jeo')}
                        value={adicionalContent}
                        onChange={(value) => setAttributes({ adicionalContent: value })}
                />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  },

  save: (props) => {
    const {
      className,
      isSelected,
      attributes: {
        title,
        subtitle,
        newsletterShortcode,
        adicionalContent,
        align,
        typeNews,
        customStyle,
      },
      setAttributes,
    } = props;

    return (
        <>
            <div className="newsletter-wrapper block-newsletter" key="container">
                <div class="category-page-sidebar">
                    <div class={`newsletter ${typeNews || 'horizontal'} ${customStyle}`} >
                    <div class="newsletter-section-header">
                        <div>
                          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" data-inline="false" data-width="1em" data-height="1em" data-icon="simple-line-icons:envelope-open" style={ { transform: 'rotate(360deg)' } }><path d="M1023 473q0-4-1-8t-2.5-8t-3.5-7.5t-5-6.5l-1-1q-2-3-5-6l-6-6L582 27Q555 0 514 0t-68 27L29 427q-6 6-12 13T5.5 458.5T0 481v479q0 27 19 45.5t45 18.5h896q17 0 32-8.5t23.5-23.5t8.5-32V478l-1-5zM270 726L64 916V545zm66 27l6-6l1-1l141-130q11-9 25-9t24 8l384 345H110zm421-22l203-180v362zM104 443L491 72q10-9 23-9t23 9l361 345h-1l55 55l-242 215l-136-121q-18-15-41-20t-47 0t-43 21L317 683L75 469l27-26h2z" fill="currentColor"></path></svg>
                        </div>
                        <div class="left">
                          <div class="newsletter-header">
                            <p><RichText.Content value={title}/></p> 
                          </div>
                          <p class="anchor-text"><RichText.Content value={subtitle}/></p>
                        </div>
                    </div>

                    <div class="input-div">
                        <InnerBlocks.Content />
                        <RichText.Content
                                tagName="p"
                                className="link-add"
                                value={adicionalContent}
                        />
                    </div>
                    </div>
                </div>
            </div>
            
        </>);
  },
});

// [mc4wp_form id="65"]
