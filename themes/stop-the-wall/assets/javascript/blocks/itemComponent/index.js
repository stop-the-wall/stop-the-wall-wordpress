import { RichText, InnerBlocks } from "@wordpress/block-editor";

import { __ } from "@wordpress/i18n";
import { Button, SelectControl, TextControl } from "@wordpress/components";
//const {  } = wp.editor;

wp.blocks.registerBlockType("jeo-theme/item-component", {
	title: __("Item component", 'jeo'),
	icon: "columns",
	category: "common",
	supports: {
		align: false,
	},
	attributes: {
		link: {
			type: 'string',
		},
	},

	edit: (props) => {
		const TEMPLATE =  [ 
				[ 'core/image' ],
				[ 'core/column', {}, [
					[ 'core/heading', { placeholder: 'Title' } ],
					[ 'core/paragraph', { placeholder: 'Description' } ],
				]]
		];
		  
		return (
			<div className="item-component">
				<div>
					<InnerBlocks
						allowedBlocks={[]}
						template={TEMPLATE}
					>
					</InnerBlocks>
					<RichText
						tagName="p"
						className="content-box-title"
						value={ props.attributes.link || '' }
						onChange={ ( newLink ) => {
							props.setAttributes( { ...props.attributes, link: newLink } );
						} }
						placeholder={ __( 'Item link', 'jeo' ) } 
					/>
				</div>
			</div>
		);
	},

	save: (props) => {
		return (
			<>	
				{ props.attributes.link && (
					<a href={ props.attributes.link } target="_blank" rel="noopener noreferrer">
						<div className="item-component">
							<InnerBlocks.Content/>
						</div>
					</a>
				) }
				{ ! props.attributes.link && (
					<div className="item-component">
						<InnerBlocks.Content/>
					</div>
				) }
			</>
		);
	},
});

// [mc4wp_form id="65"]
