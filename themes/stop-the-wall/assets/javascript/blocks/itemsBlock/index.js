import { RichText, InnerBlocks, InspectorControls } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { registerBlockType } from "@wordpress/blocks";
import { PanelBody, __experimentalNumberControl as NumberControl } from "@wordpress/components"

registerBlockType('jeo-theme/custom-items-block', {
    title: __('Items'),
    icon: 'columns',
    category: 'common',
    keywords: [
        __('Items'),
	],
	supports: {
		align: true,
	},
	attributes: {
		title: {
			type: "string",
		},
		numberOfColumns: {
			type: "number",
		},
	},

    edit(props) {
        const {
			className,
			isSelected,
			attributes: {
			  title,
			},
			setAttributes,
        } = props;

        const TEMPLATE =  [['jeo-theme/item-component']];

        return (
			<>
				<div className="items-block">
						<RichText
							tagName="h2"
							className="gallery-title"
							value={ title }
							onChange={ ( title ) => {
								setAttributes( { ...props.attributes, title } )
							} }
							placeholder={ __( 'Type a item section title', 'jeo' ) } 
						/>
						<div class="items-block__grid">
							<InnerBlocks
								allowedBlocks={[ 'jeo-theme/item-component' ]}
								template={TEMPLATE}
							/>
						</div>
						<InspectorControls>
							<PanelBody title={ __( 'Column settings', 'jeo' ) }>
								<NumberControl
									isShiftStepEnabled={ true }
									onChange={ ( newNumber ) => {
										if ( newNumber <= 0 ) {
											props.setAttributes( { ...props.attributes, numberOfColumns: 1 } );
										} else if ( newNumber >= 13 ) {
											props.setAttributes( { ...props.attributes, numberOfColumns: 12 } );
										} else {
											props.setAttributes( { ...props.attributes, numberOfColumns: parseInt( newNumber ) } );
										}
									} }
									shiftStep={ 1 }
									value={ props.attributes.numberOfColumns || 1 }
								/>
							</PanelBody>
						</InspectorControls>
				</div>
			</>
		);
    },

    save: (props) => {
        const {
			className,
			isSelected,
			attributes: {
			  title,
			},
			setAttributes,
		  } = props;

        return (
			<>	
				<div className="items-block">
                    <RichText.Content tagName="h2" value={ title } />
					<div className="items-block--content" style={ { gridTemplateColumns: `repeat(${ props.attributes.numberOfColumns || 1 }, 1fr)` } }>
						<InnerBlocks.Content/>
					</div>

					<div className="see-more">
						<p className="show">{ __( 'Show more', 'jeo' ) }</p>
						<p className="hide" style={ { display: 'none' } }>{ __( 'Show less', 'jeo' ) }</p>
					</div>
				</div>
			</>
		);

    },
});