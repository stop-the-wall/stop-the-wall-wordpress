import { RichText, InnerBlocks } from "@wordpress/block-editor";

import { __ } from "@wordpress/i18n";
import { Button, SelectControl, TextControl } from "@wordpress/components";
//const {  } = wp.editor;

wp.blocks.registerBlockType("jeo-theme/photo-content-gallery", {
	title: __( "Photo Content Gallery", 'jeo' ),
	icon: "images-alt",
    category: "common",
    keywords: [
        __('Photo', 'jeo'),
        __('Image', 'jeo'),
        __('Content', 'jeo'),
        __('Gallery', 'jeo'),
	],
	supports: {
		align: false,
	},
	attributes: {
        title: {
            type: 'string',
        },
	},

	edit: (props) => {
        const TEMPLATE =  [['jeo-theme/photo-content']];

        const {
			attributes: {
			},
        } = props;
        
		return (
			<>	
                <section class="pictures-slider">
                    <div class="pictures">
                        <button class="close-button">
                            <i class="fas fa-times"></i>
                        </button>
                        <div class="itens">
                            <InnerBlocks
								allowedBlocks={[ 'jeo-theme/photo-content' ]}
								template={TEMPLATE}
							/>
                        </div>
                    </div>

                </section>

			</>
		);
	},

	save: (props) => {
        const {
			attributes: {
			},
        } = props;

		return (
			<>	
                <section class="pictures-slider">
                    <div class="pictures">
                        <button class="close-button">
                            <i class="fas fa-compress"></i>
                        </button>
                        <div class="itens">
                            <InnerBlocks.Content/>
                        </div>
                    </div>

                </section>

			</>
		);
	},
});

