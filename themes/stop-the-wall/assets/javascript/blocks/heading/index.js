wp.domReady( () => {

	wp.blocks.registerBlockStyle( 'core/heading', [
		{
			name: 'heading-lineheight-no-bold',
			label: 'Heading with line height and no bold',
		},
		{
			name: 'heading-letter-spacing--4',
			label: 'Heading with letter spacing 4px',
		},
		{
			name: 'heading-letter-spacing--8',
			label: 'Heading with letter spacing 8px',
		},
		{
			name: 'heading-letter-spacing--12',
			label: 'Heading with letter spacing 12px',
		}
	]);
} );