import { RichText, InnerBlocks } from "@wordpress/block-editor";

import { __ } from "@wordpress/i18n";

wp.blocks.registerBlockType("jeo-theme/photo-content", {
	title: __("Photo Content"),
	icon: "format-image",
	category: "common",
	supports: {
		align: false,
	},
	attributes: {
        legend: {
            type: 'string',
        },
        location: {
            type: 'string',
        },
        photoAuthor: {
            type: 'string',
        },
        textAuthor: {
            type: 'string',
        },
        description: {
            type: 'string',
        },
	},

	edit: (props) => {
        const IMAGE_TEMPLATE =  [ 
            [ 'core/image' ],
        ];

        const {
			attributes: {
                legend,
                location,
                photoAuthor,
                textAuthor,
                description
			},
			setAttributes,
		} = props;
		  
		return (
			<div class="slider">
                <InnerBlocks
                    allowedBlocks={[]}
                    template={IMAGE_TEMPLATE}
                >
                </InnerBlocks>

                <div class="slider-row">
                    <div class="margin-auto">
                        <div class="counter"></div>
                        <RichText
                            tagName="div"
                            className="legend"
                            value={ legend }
                            onChange={ ( legend ) => {
                                return setAttributes( { ...props.attributes, legend } );
                            } }
                            placeholder={ __( 'Legend', 'jeo' ) } 
                        />
                        <div class="info">
                            <div class="location">
                                <i class="fas fa-map-marker-alt"></i>
                                <RichText
                                    tagName="p"
                                    value={ location }
                                    onChange={ ( location ) => {
                                        setAttributes( { ...props.attributes, location } )
                                    } }
                                    placeholder={ __( 'Location', 'jeo') } 
                                />
                            </div>
                            <div class="author">
                                <i class="fas fa-camera-retro"></i>
                                <RichText
                                    tagName="p"
                                    value={ photoAuthor }
                                    onChange={ ( photoAuthor ) => {
                                        setAttributes( { ...props.attributes, photoAuthor } )
                                    } }
                                    placeholder={ __( 'Author(s) of the photo' , 'jeo') } 
                                />
                            </div>
                            <div class="author-text">
                                <i class="fas fa-pencil-alt"></i>
                                <RichText
                                    tagName="p"
                                    value={ textAuthor }
                                    onChange={ ( textAuthor ) => {
                                        setAttributes( { ...props.attributes, textAuthor } )
                                    } }
                                    placeholder={ __( 'Author(s) of the text', 'jeo') } 
                                />
                            </div>
                        </div>

                        <RichText
                            tagName="div"
                            className="huge-text"
                            value={ description }
                            onChange={ ( description ) => {
                                setAttributes( { ...props.attributes, description } )
                            } }
                            placeholder={ __( 'Description', 'jeo' ) } 
                        />
                    </div>
                </div>
            </div>
		);
	},

	save: (props) => {
        const {
			attributes: {
                legend,
                location,
                photoAuthor,
                textAuthor,
                description
			},
        } = props;

		return (
			<div class="slider">
                <div class="image-bg">
                    <button type="button" data-role="none" class="single-item-collection--attachments-prev slick-arrow" aria-label="Previous" role="button" style={ { display: 'block' } }><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                    <button type="button" data-role="none" class="single-item-collection--attachments-next slick-arrow" aria-label="Next" role="button" style={ { display: 'block' } }><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    <div class="slider-row">
                        <div class="margin-auto bg-black">
                            <div class="image-cut">
                                <button class="expand-icon">
                                    <i class="fas fa-expand"></i>
                                </button>
                                <InnerBlocks.Content />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slider-row">
                    <div class="margin-auto bg-black">
                        <div class="counter"> 1 / 3 </div>
                        { legend && (
                            <div class="legend">
                                <RichText.Content tagName="div" value={ legend } />
                            </div>
                        ) }
                        { ( location || photoAuthor || textAuthor ) && (
                            <div class="info">
                                { location && (
                                    <div class="location">
                                        <i class="fas fa-map-marker-alt"></i>
                                        <RichText.Content tagName="p" value={ location } />
                                    </div>
                                ) }
                                { photoAuthor && (
                                    <div class="author">
                                        <i class="fas fa-camera-retro"></i>
                                        <RichText.Content tagName="p" value={ photoAuthor } />
                                    </div>
                                ) }
                                { textAuthor && (
                                    <div class="author-text">
                                        <i class="fas fa-pencil-alt"></i>
                                        <RichText.Content tagName="p" value={ textAuthor } />
                                    </div>
                                ) }
                            </div>
                        ) }
                        { description && (
                            <div class="huge-text">
                                <RichText.Content tagName="div" value={ description } />
                            </div>
                        ) }
                    </div>
                </div>
            </div>
		);
	},
});

// [mc4wp_form id="65"]
