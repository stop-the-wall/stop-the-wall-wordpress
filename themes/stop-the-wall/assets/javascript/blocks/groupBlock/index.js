wp.domReady( () => {

	wp.blocks.registerBlockStyle( 'core/group', [
		{
			name: 'home__block-sidebar',
			label: 'Gray column with primary color heading',
		},
		{
			name: 'all-borders-radius',
			label: 'Group with all border radius',
		},
		{
			name: 'top-borders-radius',
			label: 'Group with top border radius',
		}
	]);
} );