<?php
/**
 * The template for displaying resource page
 *
 * @package Newspack
 */
get_header();
$title = '';
?>

	<section id="primary" class="content-area">
		<div class="main-search">
			<main id="main" class="site-main">
                <div>
                    <div class="column large-8 medium-7 small-12 list ajax-posts">
                        <?php template_part('resources/archive-title', ['title' => get_archive_title()]) ?>
                        <?php template_part('resources/dynamic-posts'); ?>
                    </div>
                </div>
			</main><!-- #main -->
		</div>
		<aside class="search-page-sidebar">
    		<div class="content">
				<?php dynamic_sidebar('resources_page_sidebar') ?>
			</div>
		</aside>
	</section><!-- #primary -->

<?php
get_footer();

