<?php
require __DIR__ . '/inc/generic-css-injection.php';
require __DIR__ . '/inc/template-tags.php';
require __DIR__ . '/inc/api.php';
require __DIR__ . '/inc/post-types.php';
require __DIR__ . '/inc/custom-taxonomies.php';
require __DIR__ . '/inc/newspack-functions-overwrites.php';
require __DIR__ . '/inc/widgets.php';
require __DIR__ . '/inc/metaboxes.php';
require __DIR__ . '/inc/gutenberg-blocks.php';
require __DIR__ . '/inc/menus.php';
require __DIR__ . '/classes/ajax-pv.php';
require __DIR__ . '/classes/library_related-posts.php';



add_filter('post_link', 'custom_get_permalink', 10, 3);

function custom_get_permalink($url, $post, $leavename = false) {
	$external_source_link = get_post_meta($post->ID, 'external-source-link', true);
	if ($external_source_link) {
		return $external_source_link;
	}

	return $url;
}

add_action( 'after_setup_theme', 'jeo_setup' );

function jeo_setup() {
	load_theme_textdomain( 'jeo', get_stylesheet_directory() . '/lang' );
}

add_filter('pre_get_posts', '_search_pre_get_posts', 1);

function _search_pre_get_posts($query) {
	global $wp_query;
	//var_dump();

	if (is_admin() || !$query->is_main_query()) {
		return $query;
	}

	if (isset($query->query['p']) && strpos($query->query['p'], ':redirect') > 0) {
		$query->query['p'] = intval($query->query['p']);
		$query->is_404 = false;
		$query->is_page = true;
		$query->is_home = false;
	}


	if ($query->is_search() && $query->is_main_query()) {
		//$query->set('post_type', [$query->query['post_type']]);
		// Date filter
		if (isset($_GET['daterange'])) {
			$date_range = explode(' - ', $_GET['daterange'], 2);
			if (sizeof($date_range) == 2) {
				$from_date = date_parse($date_range[0]);
				$to_date   = date_parse($date_range[1]);
				$after  = null;
				$before = null;

				if ($from_date && checkdate($from_date['month'], $from_date['day'], $from_date['year'])) {
					$after = array(
						'year'  => $from_date['year'],
						'month' => $from_date['month'],
						'day'   => $from_date['day'],
					);
				}
				// Same for the "to" date.
				if ($to_date && checkdate($to_date['month'], $to_date['day'], $to_date['year'])) {
					$before = array(
						'year'  => $to_date['year'],
						'month' => $to_date['month'],
						'day'   => $to_date['day'],
					);
				}


				$date_query = array();
				if ($after) {
					$date_query['after'] = $after;
				}
				if ($before) {
					$date_query['before'] = $before;
				}
				if ($after || $before) {
					$date_query['inclusive'] = true;
				}

				if (!empty($date_query)) {
					$query->set('date_query', $date_query);
				}
			}
		}

		if (isset($_GET['order'])) {
			$order_option = $_GET['order'];
			$query->set('orderby', 'date');

			if ($order_option == 'ASC' || $order_option == 'DESC') {
				$query->set('order', $_GET['order']);
			}
			//var_dump($query);
		} else {
			$query->set('orderby', 'date');
			$query->set('order', 'DESC');
		}

		$categories = "";


		if(!empty($categories)) {
			$categories .= ",";
		}

		if(isset($_GET['region']) && !empty($_GET['region'])) {
			$categories .= implode(",", $_GET['region']);
		}

		if (isset($_GET['topic']) && !empty($_GET['topic'])) {
			$tags = implode(",", $_GET['topic']);
		}
		// echo $categories;

		if(!empty($categories)) {
			$query->set('category_name', $categories);
		}

		if(!empty($tags)) {
			$query->set('tag', $tags);
		}

		//var_dump($query);

	}

	return $query;
}

add_filter('pre_get_posts', 'feed_rss_filter', 2);
function feed_rss_filter($query) {
	if ($query->is_feed) {
		$query->set('meta_query', array(
			'relation' => 'OR',
			array(
				'key'     => 'external-source-link',
				'compare' => 'NOT EXISTS',
			),

			array(
				'key'     => 'external-source-link',
				'value'   => '',
				'compare' => '=',
			),
		));
	}

	return $query;
}



function ns_filter_avatar($avatar, $id_or_email, $size, $default, $alt, $args) {
	$headers = @get_headers($args['url']);
	if (!preg_match("|200|", $headers[0])) {
		return;
	}
	return $avatar;
}
add_filter('get_avatar', 'ns_filter_avatar', 10, 6);

if (!function_exists('jeo_comment_form')) {
	function jeo_comment_form() {
		comment_form([
			'logged_in_as' => null,
			'title_reply' => null,
		]);
	}
}

function remove_website_field($fields) {
	unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'remove_website_field');


// its suppose to fix (https://github.com/WordPress/gutenberg/issues/18098)
global $wp_embed;
add_filter('the_content', array($wp_embed, 'autoembed'), 9);

add_filter('comment_form_fields', 'move_comment_field');
function move_comment_field($fields) {
	$comment_field = $fields['comment'];
	unset($fields['comment']);
	$fields['comment'] = $comment_field;
	return $fields;
}


function wpseo_no_show_article_author_facebook( $facebook ) {
    if ( is_single() ) {
        return false;
    }
    return $facebook;
}
add_filter( 'wpseo_opengraph_author_facebook', 'wpseo_no_show_article_author_facebook', 10, 1 );

function get_term_for_default_lang( $term, $taxonomy ) {
	global $icl_adjust_id_url_filter_off;

	$term_id = is_int( $term ) ? $term : $term->term_id;

	$default_term_id = (int) icl_object_id( $term_id, $taxonomy, true, 'en' );

	$orig_flag_value = $icl_adjust_id_url_filter_off;

	$icl_adjust_id_url_filter_off = true;
	$term = get_term( $default_term_id, $taxonomy );
	$icl_adjust_id_url_filter_off = $orig_flag_value;

	return $term;
}

function show_publishers($id){
	if(taxonomy_exists('partner')){
		$partners = get_the_terms( get_the_id(), 'partner');
		if ($partners && count($partners) > 0){
			$partner_link = get_post_meta($id, 'partner-link', true); 
			if (class_exists('WPSEO_Primary_Term')) {
				$wpseo_primary_term = new WPSEO_Primary_Term( 'partner', get_the_id() );
				$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
				$term = get_term( $wpseo_primary_term );

				if ($term || count($partners) == 1 ) {

					$partner_name = '';
					if($term) {
						$partner_name = $term->name;
					} else if (count($partners) == 1) {
						$partner_name = $partners[0]->name;
					}

					?>
					<div class="publishers">
									<span class="publisher-name">
										<?php echo esc_html__('By', 'jeo'); ?>
										<a href="<?= $partner_link ?>" >
											<i class="fas fa-sync-alt publisher-icon"></i>
											<?php echo $partner_name; ?>
										</a>
									</span>
							</div>
							<?php 
				} 
			}
		}
	}
}

add_filter('pre_get_posts', 'resources_search_pre_get_posts', 1);

function get_archive_title () {
	$s = isset($_GET['s']) ? trim($_GET['s']) : '';

	if($s){
		if (is_tag()) {
			$title = sprintf(__('Busca por "%s" na tag "%s"', 'ods'), $s, single_tag_title('',false));
		} elseif (is_category()) {
			$title = sprintf(__('Busca por "%s" na categoria "%s"', 'ods'), $s, single_cat_title('',false));
		} elseif (is_tax()) {
			$title = sprintf(__('Busca por "%s" em "%s"', 'ods'), $s, single_term_title('',false));
		} else {
			if(is_archive()){
				$title = sprintf(__('Busca por "%s" em "%s"', 'ods'), $s, post_type_archive_title());
			} else {
				$title = sprintf(__('Resultado da busca por "%s"', 'ods'), $s);
			}
		}
	} else {
		if (is_tag()) {
			$title = single_tag_title('Tag: ',false);
		} elseif (is_category()) {
			$title = single_cat_title('Categoria: ',false);
		} elseif (is_tax()) {
			$title = single_term_title('',false);
		} else {
			$title = post_type_archive_title('', false);
		}
	}

	return $title;
}

function template_part($template_name, $params = []){
    global $wp_query;
    $template_filename = locate_template("template-parts/{$template_name}.php");

    if(empty($template_filename)){
        return;
    }

    extract($params);

    include $template_filename;;
}

function resources_search_pre_get_posts($query) {
	if (is_admin() || !$query->is_main_query()) {
		return $query;
	}

	if ($query->is_search() && $query->is_main_query()) {
		//$query->set('post_type', [$query->query['post_type']]);
        
		// Date filter
		if (isset($_GET['daterange'])) {
			$date_range = explode(' - ', $_GET['daterange'], 2);
			if (sizeof($date_range) == 2) {
				$from_date = date_parse($date_range[0]);
                $to_date   = date_parse($date_range[1]);
				$after  = null;
				$before = null;

				if ($from_date && checkdate($from_date['month'], $from_date['day'], $from_date['year'])) {
					$after = array(
						'year'  => $from_date['year'],
						'month' => $from_date['month'],
						'day'   => $from_date['day'],
					);
				}
				// Same for the "to" date.
				if ($to_date && checkdate($to_date['month'], $to_date['day'], $to_date['year'])) {
					$before = array(
						'year'  => $to_date['year'],
						'month' => $to_date['month'],
						'day'   => $to_date['day'],
					);
				}

				$date_query = array();
				if ($after) {
					$date_query['after'] = $after;
				}
				if ($before) {
					$date_query['before'] = $before;
				}
				if ($after || $before) {
					$date_query['inclusive'] = true;
				}

				if (!empty($date_query)) {
					$query->set('date_query', $date_query);
				}
			}
		}

        // order
        $query->set('order', 'DESC');
        $query->set('orderby', 'date');

        if (isset($_GET['order'])) {  
            if ($_GET['order'] == 'title') {
                $query->set('order', 'ASC');
                $query->set('orderby', 'title');
            } elseif ($_GET['order'] == 'date') {
                $query->set('order', 'DESC');
                $query->set('orderby', 'date');
            }	
        }

        // categoria e midia
        if (isset($_GET['midia_filter']) && !empty($_GET['midia_filter']) && isset($_GET['category_filter']) && !empty($_GET['category_filter']) ) {            
            $tax_query = [
                'relation' => 'AND',
                [
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => $_GET['category_filter']
                ],
                [
                    'taxonomy' => 'midia',
                    'field'    => 'slug',
                    'terms'    => $_GET['midia_filter']
                ]
            ];

            $query->set('tax_query', $tax_query);

        } else {
            if (isset($_GET['category_filter']) && !empty($_GET['category_filter'])) {
                $query->set('category_name', $_GET['category_filter']);
            }
            
            if (isset($_GET['midia_filter']) && !empty($_GET['midia_filter'])) {
                $query->set('tax_query', [
                    array(
                        'taxonomy' => 'midia',
                        'field'    => 'slug',
                        'terms'    => $_GET['midia_filter']
                    ),
                ]);

            }
        }
        //var_dump($query);

    }
    
    return $query;
    
}

function loadmore_ajax_handler(){
	// prepare our arguments for the query
    $args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_status'] = 'publish';
    
	// it is always better to use WP_Query but not here
	global $sitepress;
 
    $lang = $_POST['language']; 
    $sitepress->switch_lang( $lang );

    query_posts( $args );

	if( have_posts() ) :
		// run the loop
        while( have_posts() ):
            the_post();
            global $post;
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			template_part('resources/card', []);
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 
 		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
  
add_action('wp_ajax_loadmore', 'loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler');


/**
 * Retorna a quantidade de posts de acordo com os parâmetros enviados
 *
 * @param array     $post_type
 * @param string    $taxonomy
 * @param array     $terms
 * @param array     $status
 * @return int
 */

function get_count_posts($post_type = ['post'], $taxonomy = '', $terms = [], $status = ['publish']) {
    
    $args = [
        'post_type'      => $post_type,
        'post_status'    => $status, 
        'posts_per_page' => -1,
        'tax_query' => [
            [
                'taxonomy' => $taxonomy,
                'terms'    => $terms
            ]
        ]
    ];

    $query = new \WP_Query($args);

    return (int) $query->post_count;

}

function get_excerpt($content = '', $limit = '', $after = '')
{

    if ($limit) {
        $l = $limit;
    } else {
        $l = '240';
    }

    if ($content) {
        $excerpt = $content;
    } elseif(has_excerpt()) {
        return get_the_excerpt();
    } else {
        $excerpt = get_the_content();
    }

    $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $l);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));

    if ($after) {
        $a = $after;
    } else {
        $a = '...';
    }

    if (!empty($excerpt)) {
        $excerpt = $excerpt . $a;
        return $excerpt;
    }

}

function enqueue_assets()
{
    wp_register_style( 'slick-css', get_stylesheet_directory_uri() . '/assets/vendor/slick/css/slick.min.css', '', '1.6.1', '' );
    wp_register_style( 'slick-theme-css', get_stylesheet_directory_uri() . '/assets/vendor/slick/css/slick-theme.min.css', '', '1.6.1', '' );
    wp_enqueue_style( 'slick-css' );
    wp_enqueue_style( 'slick-theme-css' );
    wp_register_script( 'slick-js', get_stylesheet_directory_uri() . '/assets/vendor/slick/js/slick.min.js', array( 'jquery' ), '1.6.1', true );
	wp_enqueue_script( 'slick-js' );
}

add_action('wp_enqueue_scripts', 'enqueue_assets');


add_filter( 'wp_enqueue_scripts', 'unregister_amp_menu',10000);
function unregister_amp_menu() {
	wp_dequeue_script('newspack-amp-fallback');
	wp_deregister_script('newspack-amp-fallback');
}


add_filter('template_include', 'overwrite_petitions_single_template');

function overwrite_petitions_single_template($template) {
	if (is_singular('petition')) {
		return __DIR__ . '/single-petition.php';
	}
	return $template;
}

// function shapeSpace_filter_search($query) {
// 	if (!$query->is_admin && $query->is_search) {
// 		$query->set('post_type', array('post', 'page'));
// 	}
// 	return $query;
// }
// add_filter('pre_get_posts', 'shapeSpace_filter_search');