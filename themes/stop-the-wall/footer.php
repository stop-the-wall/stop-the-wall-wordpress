<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newspack
 */

?>

	<?php do_action( 'before_footer' ); ?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="header-footer header__wrapper">
			<div class="search-footer">
				<?php get_search_form(); ?>
			</div>
			<div class="logo">
				<div class="image"><?php newspack_the_sticky_logo(); ?></div>
				<p class="text1"><?= __('PALESTINIAN GRASSROOTS', 'jeo'); ?></p>
				<p class="text2"><?= __('ANTI-APARTHEID WALL', 'jeo'); ?></p>
				<p class="text3"><?= __('CAMPAIGN', 'jeo'); ?></p>
			</div>
			<div class="follow-us">
				<p class="text"><?= __('FOLLOW US:', 'jeo') ?></p>
				<?php newspack_social_menu_header(); ?>
			</div>
		</div>
		<div class="body body__wrapper">
			<div class="column1 column">
				<?php if ( is_active_sidebar( 'footer_column1_sidebar' )  ) : ?>
					<?php dynamic_sidebar( 'footer_column1_sidebar' ); ?>
				<?php endif; ?>
			</div>
			<div class="column2 column">
				<?php if ( is_active_sidebar( 'footer_column2_sidebar' )  ) : ?>
					<?php dynamic_sidebar( 'footer_column2_sidebar' ); ?>
				<?php endif; ?>
			</div>
			<div class="column3 column">
				<?php if ( is_active_sidebar( 'footer_column3_sidebar' )  ) : ?>
					<?php dynamic_sidebar( 'footer_column3_sidebar' ); ?>
				<?php endif; ?>
			</div>
			<div class="column4 column">
				<?php if ( is_active_sidebar( 'footer_column4_sidebar' )  ) : ?>
					<?php dynamic_sidebar( 'footer_column4_sidebar' ); ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="website-credits">
			<p class="text1">
				<?=  __('website by ', 'jeo'); ?>
			</p>
			<a href="https://hacklab.com.br">
				<p class="text2">
					hacklab
				</p>
				<p class="text3">
					/
				</p>
			</a>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
